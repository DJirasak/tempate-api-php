<?php
class Sample{
    private $conn = null;
    function __construct(){
        include '../config/response.php';
        require "../config/database.php";
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    function isOddNum($num){
        try {
            if($num % 2 == 0){
                responseJson(400, 'This Number is Even', null);
            }else{
                responseJson(200, 'This Number is Odd', null);
            }
        }catch(Exception $err){ 
            responseJson(500, $err->getMessage(), null);
        }
    }

    function __destruct(){
        $this->conn = null;
    }
} 


?>